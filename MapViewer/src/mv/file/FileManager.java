/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.awt.Polygon;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Polyline;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;


/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    
    static final String NUMBER_OF_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    static final String SUBREGIONS = "SUBREGIONS";
    static final String NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    static final String SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        
        //CLEAR THE OLD DATA
        DataManager dataManager = (DataManager)data;
	dataManager.reset();
        
        
        JsonObject json = loadJSONFile(filePath);
        JsonArray jsonSubregionsArray = json.getJsonArray(SUBREGIONS);
        
        for (int i = 0; i < jsonSubregionsArray.size(); i++) 
        {

            JsonObject json1 = jsonSubregionsArray.getJsonObject(i);
            JsonArray jsonSubregionsPolygonsArray = json1.getJsonArray(SUBREGION_POLYGONS);
            
           
            for(int j = 0; j < jsonSubregionsPolygonsArray.size(); j++)
            {
                JsonArray items = jsonSubregionsPolygonsArray.getJsonArray(j);
                
                //System.out.println(items.size());
                //System.out.println(jsonItem.size());
                dataManager.subregions_polygon.add(items.size());
                //System.out.println(items);
                dataManager.subregions.add(j);
                for(int k = 0; k  < items.size(); k++)
                {
                    
                    JsonObject jsonItem = items.getJsonObject(k);
                    dataManager.addX(getDataAsDouble(jsonItem,"X"));
                    dataManager.addY(getDataAsDouble(jsonItem,"Y"));
                    
                    //System.out.println(dataManager.subregions.size());
                }
            
            }
        }
        //dataManager.subregions.add(jsonSubregionsPolygonsArray.size());
        
        
        
        
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
