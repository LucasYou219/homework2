package mv.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import saf.components.AppDataComponent;
import mv.MapViewerApp;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    
    public ArrayList <Double> arrayX = new ArrayList<Double>();
    public ArrayList <Double> arrayY = new ArrayList<Double>();
    public ArrayList subregions = new ArrayList<>();
    public ArrayList subregions_polygon = new ArrayList<>();
    public ArrayList <Double> coordinateX = new ArrayList<Double>();
    public ArrayList <Double> coordinateY = new ArrayList<Double>();
    
    public DataManager(MapViewerApp initApp) 
    {
        app = initApp;
   
    }
    public void addX(Double X) {
        arrayX.add(X);
    } 
    public void addY(Double Y) {
        arrayY.add(Y);
    } 
    
    public ArrayList<Double> getX()
    {
        return arrayX;
    }
    
    public ArrayList<Double> getY()
    {
        return arrayY;
    }
    @Override
    public void reset() 
    {
        arrayX.clear();
        arrayY.clear();
        subregions.clear();
        subregions_polygon.clear();
        coordinateX.clear();
        coordinateY.clear();
        
    }
}
