/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;


import java.awt.Paint;
import java.util.ArrayList;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import saf.ui.AppGUI; 
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Polygon;
import javafx.scene.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.*;

import static javafx.scene.paint.Color.LIGHTGREEN;
import static javafx.scene.paint.Color.LIGHTGREY;
import static javafx.scene.paint.Color.WHITE;
import static javafx.scene.paint.Color.color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import mv.data.DataManager;
/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    
    AppGUI gui;
    double moveX = 0.0;
    double moveY = 0.0;
    //ArrayList <Double> coordinateX = new ArrayList<Double>();
    //ArrayList <Double> coordinateY = new ArrayList<Double>();
    int count = 0;
    int G = 0;
    int M = 0;
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        workspace = new Pane();
        gui = app.getGUI();
       
        DataManager dataManager = (DataManager)app.getDataComponent();
        ((FlowPane)gui.getAppPane().getTop()).getChildren().remove(0);
        ((FlowPane)gui.getAppPane().getTop()).getChildren().remove(1);
        

        dataManager.reset();
    }

    @Override
    public void reloadWorkspace() {
        workspace.getChildren().clear();
        DataManager dataManager = (DataManager)app.getDataComponent();
        BorderPane borderpane= new BorderPane();
        BorderPane togglepane= new BorderPane();
        BorderPane dotpane = new BorderPane();
        borderpane.getChildren().add(togglepane);
        borderpane.getChildren().add(dotpane);
        workspace.getChildren().add(borderpane);
        double x = 0.0;
        double y = 0.0;
        for(int c = 0; c < dataManager.arrayX.size();c++)
        {
            x = (reScaleX(180 + dataManager.getX().get(c)) * gui.getPrimaryScene().getWidth());
            y = (reScaleY(90 - dataManager.getY().get(c)) * gui.getPrimaryScene().getHeight()) -25;
            dataManager.coordinateX.add(x);
            dataManager.coordinateY.add(y);
        }
        for(int i = 0; i < dataManager.subregions.size(); i++)
        {
           
            Polyline polygon = new Polyline();
            int b =  (int) dataManager.subregions_polygon.get(i);
            
            for(int j = 0; j < b; j++)
            {
                polygon.getPoints().addAll(dataManager.coordinateX.get(count),dataManager.coordinateY.get(count));
                count++;
            }
            polygon.setFill(LIGHTGREEN);
            Rectangle clip = new Rectangle(gui.getPrimaryScene().getWidth()+30, gui.getPrimaryScene().getHeight());

            workspace.setStyle("-fx-background-color: #31A5BF");
            workspace.setClip(clip);
            borderpane.getChildren().add(polygon);
            app.getGUI().getAppPane().setCenter(workspace);
            polygon.setOnMousePressed(e ->{
                if(e.getButton() == MouseButton.PRIMARY)
                {
                    //System.out.println("LEFT CLICK --> ZOOM IN");

                    borderpane.setLayoutX(borderpane.getLayoutX() + (app.getGUI().getPrimaryScene().getWidth()/2-e.getSceneX())*1.1);
                    borderpane.setLayoutY(borderpane.getLayoutY() + ((app.getGUI().getPrimaryScene().getHeight())/2-e.getSceneY())*1.1);
                    borderpane.setScaleX(borderpane.getScaleX() * 1.1);
                    borderpane.setScaleY(borderpane.getScaleY() * 1.1);

                    
                    
                }
                
                else if (e.getButton() == MouseButton.SECONDARY)
                {
                    borderpane.setLayoutX(borderpane.getLayoutX() + (app.getGUI().getPrimaryScene().getWidth()/2-e.getSceneX())/1.1);
                    borderpane.setLayoutY(borderpane.getLayoutY() + ((app.getGUI().getPrimaryScene().getHeight())/2-e.getSceneY())/1.1);
                    borderpane.setLayoutX(borderpane.getLayoutX() + (app.getGUI().getPrimaryScene().getWidth()/2- (app.getGUI().getPrimaryScene().getWidth()/1.15/2)/1.1));
                    borderpane.setLayoutY(borderpane.getLayoutY() + (app.getGUI().getPrimaryScene().getHeight()/2-(app.getGUI().getPrimaryScene().getHeight()/1.15/2)/1.1));
                    borderpane.setScaleX(borderpane.getScaleX() /1.1);
                    borderpane.setScaleY(borderpane.getScaleY() /1.1);

                    
          

                }
                    
            });
            gui.getAppPane().setFocusTraversable(true);
            gui.getAppPane().setOnKeyPressed(e -> {
                switch (e.getCode())
                {
                    case UP : 
                        moveY = moveY + 15;
                        borderpane.setLayoutY(moveY); 
                        break;
                    case DOWN : 
                        moveY = moveY - 15;
                        borderpane.setLayoutY(moveY);
                        
                        break;
                    case LEFT : 
                        moveX = moveX + 15;
                        borderpane.setLayoutX(moveX);
                        break;
                    case RIGHT : 
                        moveX = moveX - 15;
                        borderpane.setLayoutX(moveX);
                        break;
                    case G:
                        if(G % 2 == 0)
                        {
                            G++;
                            Line Equator = new Line();
                            Equator.setStartX(0);
                            Equator.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            Equator.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            Equator.setEndY(app.getGUI().getPrimaryScene().getHeight()/2-30);
                            Equator.setStroke(WHITE);
                            togglepane.getChildren().add(Equator);
                            
                            Line PrimeMeridian = new Line();
                            PrimeMeridian.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            PrimeMeridian.setEndX(app.getGUI().getPrimaryScene().getWidth()/2);
                            PrimeMeridian.setStartY(0);
                            PrimeMeridian.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            PrimeMeridian.setStroke(WHITE);
                            togglepane.getChildren().add(PrimeMeridian);
                            
                            Line InternationalDateLineLeft = new Line();
                            InternationalDateLineLeft.setStartX(0);
                            InternationalDateLineLeft.setEndX(0);
                            InternationalDateLineLeft.setStartY(0);
                            InternationalDateLineLeft.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            InternationalDateLineLeft.setStroke(WHITE);
                            togglepane.getChildren().add(InternationalDateLineLeft);
                            
                            Line InternationalDateLineRight = new Line();
                            InternationalDateLineRight.setStartX(app.getGUI().getPrimaryScene().getWidth());
                            InternationalDateLineRight.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            InternationalDateLineRight.setStartY(0);
                            InternationalDateLineRight.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            InternationalDateLineRight.setStroke(WHITE);
                            togglepane.getChildren().add(InternationalDateLineRight);
                            
                            Line line1 = new Line();
                            line1.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line1.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            
                            line1.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            line1.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 - (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line1.getStrokeDashArray().addAll(15d);
                            line1.setStrokeWidth(0.5);
                            line1.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line1);
                            
                            Line line2 = new Line();
                            line2.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line2.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line2.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) + ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line2.setEndY(0);
                            line2.getStrokeDashArray().addAll(15d);
                            line2.setStrokeWidth(0.5);
                            line2.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line2);
                            
                            Line line3 = new Line();
                            line3.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line3.setEndX(0);
                            line3.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line3.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 - (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line3.getStrokeDashArray().addAll(15d);
                            line3.setStrokeWidth(0.5);
                            line3.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line3);
                            
                            Line line4 = new Line();
                            line4.setStartX(app.getGUI().getPrimaryScene().getWidth()/2); 
                            line4.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            
                            line4.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) - ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line4.setEndY(0);
                            line4.getStrokeDashArray().addAll(15d);
                            line4.setStrokeWidth(0.5);
                            line4.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line4);
                            
                          
                            //FORTH
                            Line line5 = new Line();
                            line5.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line5.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            line5.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line5.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 + (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line5.getStrokeDashArray().addAll(15d);
                            line5.setStrokeWidth(0.5);
                            line5.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line5);
                            
                            Line line6 = new Line();
                            line6.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);                          
                            line6.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            
                            line6.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) + ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line6.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            line6.getStrokeDashArray().addAll(15d);
                            line6.setStrokeWidth(0.5);
                            line6.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line6);
                            
                            Line line7 = new Line();
                            line7.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line7.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            line7.setEndX(0);
                            line7.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line7.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 + (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line7.getStrokeDashArray().addAll(15d);
                            line7.setStrokeWidth(0.5);
                            line7.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line7);
                            
                            Line line8 = new Line();
                            line8.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            ;
                            line8.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line8.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) - ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line8.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            line8.getStrokeDashArray().addAll(15d);
                            line8.setStrokeWidth(0.5);
                            line8.setStroke(LIGHTGREY);
                            togglepane.getChildren().add(line8);
                            

                            
                        }
                        else if (G % 2 != 0)
                        {
                            G++;
                            //System.out.println("off");
                            togglepane.getChildren().clear();
                        }
                        break;
                    case M:
                        if(M % 2 == 0)
                        {
                            M++;
                            Line Equator = new Line();
                            Equator.setStartX(0);
                            Equator.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            Equator.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            Equator.setEndY(app.getGUI().getPrimaryScene().getHeight()/2-30);
                            Equator.getStrokeDashArray().addAll(15d);
                            Equator.setStrokeWidth(0.5);
                            Equator.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(Equator);
                            
                            Line PrimeMeridian = new Line();
                            PrimeMeridian.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            PrimeMeridian.setEndX(app.getGUI().getPrimaryScene().getWidth()/2);
                            PrimeMeridian.setStartY(0);
                            PrimeMeridian.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            PrimeMeridian.getStrokeDashArray().addAll(15d);
                            PrimeMeridian.setStrokeWidth(0.5);
                            PrimeMeridian.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(PrimeMeridian);
                            
                           
                            
                            Line line1 = new Line();
                            line1.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line1.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line1.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            line1.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 - (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line1.getStrokeDashArray().addAll(15d);
                            line1.setStrokeWidth(0.5);
                            line1.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line1);
                            
                            Line line2 = new Line();
                            line2.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line2.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line2.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) + ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line2.setEndY(0);
                            line2.getStrokeDashArray().addAll(15d);
                            line2.setStrokeWidth(0.5);
                            line2.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line2);
                            
                            Line line3 = new Line();
                            line3.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line3.setEndX(0);
                            line3.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line3.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 - (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line3.getStrokeDashArray().addAll(15d);
                            line3.setStrokeWidth(0.5);
                            line3.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line3);
                            
                            Line line4 = new Line();
                            line4.setStartX(app.getGUI().getPrimaryScene().getWidth()/2); 
                            line4.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            
                            line4.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) - ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line4.setEndY(0);
                            line4.getStrokeDashArray().addAll(15d);
                            line4.setStrokeWidth(0.5);
                            line4.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line4);
                            
                          
                            //FORTH
                            Line line5 = new Line();
                            line5.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line5.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            line5.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line5.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 + (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line5.getStrokeDashArray().addAll(15d);
                            line5.setStrokeWidth(0.5);
                            line5.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line5);
                            
                            Line line6 = new Line();
                            line6.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);                          
                            line6.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            
                            line6.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) + ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line6.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            line6.getStrokeDashArray().addAll(15d);
                            line6.setStrokeWidth(0.5);
                            line6.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line6);
                            
                            Line line7 = new Line();
                            line7.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line7.setEndX(app.getGUI().getPrimaryScene().getWidth());
                            line7.setEndX(0);
                            line7.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line7.setEndY(app.getGUI().getPrimaryScene().getHeight()/2 + (0.577 * app.getGUI().getPrimaryScene().getHeight()/2));
                            line7.getStrokeDashArray().addAll(15d);
                            line7.setStrokeWidth(0.5);
                            line7.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line7);
                            
                            Line line8 = new Line();
                            line8.setStartX(app.getGUI().getPrimaryScene().getWidth()/2);
                            line8.setStartY(app.getGUI().getPrimaryScene().getHeight()/2 -30);
                            line8.setEndX((app.getGUI().getPrimaryScene().getWidth()/2) - ((app.getGUI().getPrimaryScene().getWidth()/2)*0.577));
                            line8.setEndY(app.getGUI().getPrimaryScene().getHeight()-30);
                            line8.getStrokeDashArray().addAll(15d);
                            line8.setStrokeWidth(0.5);
                            line8.setStroke(LIGHTGREY);
                            dotpane.getChildren().add(line8);
                            

                            
                        }
                        else if (M % 2 != 0)
                        {
                            M++;
                            dotpane.getChildren().clear();
                        }
                        break;
                }
        });
            
            
            
        }
 
        count = 0;
        G = 0;  
 

        
    }

    @Override
    public void initStyle() {
        
    }


    
    private double reScaleX(double x)
    {
        x = x/360;
        
        return x;
    }
    
    private double reScaleY(double y)
    {
        y = y/180;

        return y;
    }
}